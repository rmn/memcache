<?php
    /*
        MemCache

        Redis  数据库


        C/S  结构  
            有服务端  
            有客户端

        memcached.exe -d install  安装进系统服务    记得启动服务
        memcached.exe -d uninstall 卸载


        客户端：
            命令行连接  telnet 127.0.0.1 11211
            putty  协议选择 telnet  端口设置为 11211

            用PHP作为客户端
    */

    $mem = new Memcache;

    //建立连接
    $mem->connect('127.0.0.1','11211');

    //村数据
    //$mem->set('name','jack');


    //取数据
    $result = $mem->get('name');
    echo $result;
?>
